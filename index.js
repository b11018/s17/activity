/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function askInvasiveQuestions(){
		let userName = prompt("Hello! What's your full name?");
		let userAge = prompt("How old are you?");
		let userCity = prompt("Which city do you live in?");
		console.log("Hi, " + userName + ".");
		console.log("You are " + userAge + " years old.");
		console.log("You live in " + userCity + " City.");
	};

	askInvasiveQuestions();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function nameFaveBands(){
		let topOneBand = ("1. Snarky Puppy");
		let	topTwoBand = ("2. Slipknot");
		let	topThreeBand = ("3. Extreme");
		let	topFourBand = ("4. Mr. Big");
		let	topFiveBand = ("5. The Winery Dogs");
			
		console.log(topOneBand);
		console.log(topTwoBand);
		console.log(topThreeBand);
		console.log(topFourBand);
		console.log(topFiveBand);
	}
	nameFaveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function nameFaveMovies(){
		let topOneMovie = ("1. Shaun of the Dead");
		let topOneRating = ("92%");
		let	topTwoMovie = ("2. Hot Fuzz");
		let topTwoRating = ("91%");
		let	topThreeMovie = ("3. Fight Club");
		let topThreeRating = ("91%");
		let	topFourMovie = ("4. Snatch");
		let topFourRating = ("74%");
		let	topFiveMovie = ("5. RocknRolla");
		let topFiveRating = ("60%");
			
		console.log(topOneMovie);
		console.log("Rotten Tomatoes Rating: " + topOneRating);
		console.log(topTwoMovie);
		console.log("Rotten Tomatoes Rating: " + topTwoRating);
		console.log(topThreeMovie);
		console.log("Rotten Tomatoes Rating: " + topThreeRating);
		console.log(topFourMovie);
		console.log("Rotten Tomatoes Rating: " + topFourRating);
		console.log(topFiveMovie);
		console.log("Rotten Tomatoes Rating: " + topFiveRating);
	}
	nameFaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);